#pragma once

#include <array>
#include <memory>

#include "engine.hxx"

class graph_obj
{
public:
    virtual bool init (potap::engine* e,
                      const std::string& path_to_tex,
                      const std::string& path_to_vert_buff) = 0;


    virtual void update(const potap::vec2& p,
                        const float& asp,
                        const float& sc,
                        const float& t) = 0;

    virtual bool draw() = 0;

};
