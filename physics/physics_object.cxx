#include "./physics_object.hxx"

#include <algorithm>
#include <cmath>
#include <iostream>

physics_object::~physics_object(){}

int physics_object::add_new_gravity_point(const gravity_point& p)
{
    points.push_back(p);
    return 0;
}

int physics_object::add_new_force(const force &f)
{
    forces.push_back(f);
    return 0;
}

double physics_object::calculate_distance(const gravity_point &gp)
{
    return sqrt(pow(coord.x - gp.coord.x, 2) + pow(coord.y - gp.coord.y, 2));
}

force physics_object::gravity_force_calculating(const gravity_point& gp)
{
    force tmp;

    double dist_x = gp.coord.x - coord.x;
    double dist_y = gp.coord.y - coord.y;
    double dist = std::sqrt(std::pow(dist_x, 2) + std::pow(dist_y, 2));

    tmp.angle = atan2(dist_y, dist_x);
    tmp.size = GRAVITY_CONST * gp.mass * mass / dist; // !!! works
    tmp.beg.x = coord.x;
    tmp.beg.y = coord.y;
    //redefine x&y shifts in order to force size
    tmp.end.x = tmp.size * std::cos(tmp.angle);
    tmp.end.y = tmp.size * std::sin(tmp.angle);

    //double g = GRAVITY_CONST * gp.mass / pow(tmp.dist, 2);

    return tmp;
}

int physics_object::update()
{
    forces.clear();

    for (auto x : points){
        forces.push_back(gravity_force_calculating(x));
    }

    f = forces[0]; // f - object of the "force" class
    for (size_t i = 1; i < forces.size(); i++){
        // (!!!) sums all the forces, which this object feels
        // it is overloaded operator+ of force class
        f = f + forces[i];
    }

    return 0;
}

void physics_object::print_forces()
{
    std::cout << " force      " << "| angle " << std::endl;
    std::cout << "------------" << "--------" << std::endl;
    for(auto x : forces){
        std::cout << x.size << " | " << x.get_angle_grad() << std::endl;
    }
    std::cout << "-----------------------" << std::endl;
    std::cout << f.size << " | " << f.get_angle_grad() << std::endl;
}
