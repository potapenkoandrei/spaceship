#pragma once

#include <iosfwd>
#include <string>
#include <string_view>
#include <variant>
#include <memory>

#ifndef potap_DECLSPEC
#define potap_DECLSPEC
#endif

namespace potap
{

struct potap_DECLSPEC vec2
{
    vec2();
    vec2(float x, float y);
    float x = 0;
    float y = 0;
};

potap_DECLSPEC vec2 operator+(const vec2& l, const vec2& r);

struct potap_DECLSPEC mat2x3
{
    mat2x3();
    static mat2x3 identiry();
    static mat2x3 scale(float scale);
    static mat2x3 scale(float sx, float sy);
    static mat2x3 rotation(float thetha);
    static mat2x3 move(const vec2& delta);
    vec2          col0;
    vec2          col1;
    vec2          delta;
};

potap_DECLSPEC vec2 operator*(const vec2& v, const mat2x3& m);
potap_DECLSPEC mat2x3 operator*(const mat2x3& m1, const mat2x3& m2);

/// dendy gamepad emulation events
enum class event
{
    /// input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    enter_pressed,
    enter_released,
    escape_pressed,
    escape_released,
    l_ctrl_pressed,
    l_ctrl_released,
    space_pressed,
    space_released,
    mouse_motion, //move
    mouse_pressed,
    mouse_released,
    /// virtual console events
    turn_off
};

potap_DECLSPEC std::ostream& operator<<(std::ostream& stream, const event e);

enum class keys
{
    left,
    right,
    up,
    down,
    enter,
    escape,
    l_ctrl,
    space,
    mouse
};

struct mouse_pos {
    mouse_pos()
        : x{ 0 }
        , y{ 0 }
    {
    }

    mouse_pos(const size_t& x_, const size_t& y_)
        : x{ x_ }
        , y{ y_ }
    {
    }

    size_t x = 0;
    size_t y = 0;
};

class engine;

/// return not null on success
potap_DECLSPEC engine* create_engine();
potap_DECLSPEC void    destroy_engine(engine* e);

class potap_DECLSPEC color
{
public:
    color() = default;
    explicit color(std::uint32_t rgba_);
    color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

/// vertex with position only
struct potap_DECLSPEC v0
{
    vec2 pos;
};

/// vertex with position and texture coordinate
struct potap_DECLSPEC v1
{
    vec2  pos;
    color c;
};

/// vertex position + color + texture coordinate
struct potap_DECLSPEC v2
{
    vec2  pos;
    vec2  uv;
    color c;
};

/// triangle with positions only
struct potap_DECLSPEC tri0
{
    tri0();
    v0 v[3];
};

/// triangle with positions and color
struct potap_DECLSPEC tri1
{
    tri1();
    v1 v[3];
};

/// triangle with positions color and texture coordinate
struct potap_DECLSPEC tri2
{
    tri2();
    v2 v[3];
};

//potap_DECLSPEC std::ostream& operator<<(std::ostream& stream, const input_data&);
//potap_DECLSPEC std::ostream& operator<<(std::ostream& stream,
//                                     const hardware_data&);
//potap_DECLSPEC std::ostream& operator<<(std::ostream& stream, const event& e);

potap_DECLSPEC std::istream& operator>>(std::istream& is, mat2x3&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, vec2&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, color&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, v0&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, v1&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, v2&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, tri0&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, tri1&);
potap_DECLSPEC std::istream& operator>>(std::istream& is, tri2&);

class potap_DECLSPEC texture
{
public:
    virtual ~texture();
    virtual std::uint32_t get_width() const  = 0;
    virtual std::uint32_t get_height() const = 0;
};

class potap_DECLSPEC vertex_buffer
{
public:
    virtual ~vertex_buffer();
    virtual const v2* data() const = 0;
    /// count of vertexes
    virtual size_t size() const = 0;
};

class sound_buffer
{
public:
    enum class properties
    {
        once,
        looped,
        stop
    };

    virtual ~sound_buffer();
    virtual void play(const properties) = 0;

    bool                       is_playing = false;
    bool                       is_looped  = false;
    bool                       is_stop  = false;
};


class potap_DECLSPEC engine
{
public:
    virtual ~engine();
    /// create main window
    /// on success return empty string
    virtual std::string initialize(std::string_view config) = 0;
    /// return seconds from initialization
    virtual float get_time_from_init() = 0;
    /// pool event from input queue
    virtual bool read_event(event& e)        = 0;
    virtual bool is_key_down(const potap::keys) = 0;

    virtual texture* create_texture(std::string_view path) = 0;
    virtual void destroy_texture(texture* t)               = 0;

    virtual vertex_buffer* create_vertex_buffer(const tri2*, std::size_t) = 0;
    virtual void           destroy_vertex_buffer(vertex_buffer*) = 0;

    virtual sound_buffer* create_sound_buffer(std::string_view path) = 0;

    virtual void destroy_sound_buffer(sound_buffer*)                 = 0;

    virtual void render(const tri0&, const color&) = 0;
    virtual void render(const tri1&) = 0;
    virtual void render(const tri2&, texture*) = 0;
    virtual void render(const tri2&, texture*, const mat2x3& m)        = 0;
    virtual void render(const vertex_buffer&, texture*, const mat2x3&) = 0;
    virtual void swap_buffers() = 0;
    virtual void uninitialize() = 0;
    virtual mouse_pos get_mouse() = 0;

    mouse_pos mouse_coord;
    mouse_pos mouse_coord_pressed;
    mouse_pos mouse_coord_released;
};

} // end namespace om
