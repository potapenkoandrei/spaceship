#include <iostream>
#include <fstream>
#include <cmath>

#include "static_object.hxx"

bool static_object::init(potap::engine* e,
                  const std::string& path_to_tex,
                  const std::string& path_to_vert_buff)
{
    engine = e;
    set_texture(path_to_tex);
    set_vertex_buffer(path_to_vert_buff);
    visible = true;
    return EXIT_SUCCESS;
}

bool static_object::set_texture(const std::string &t)
{
    tex = engine->create_texture(t);
    if (nullptr == tex)
    {
        std::cerr << "failed load texture " << t << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool static_object::set_vertex_buffer(const std::string &path)
{
    buf = nullptr;

    std::ifstream file(path);
    if (!file)
    {
        std::cerr << "can't load " << path << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        file >> tr[0] >> tr[1];
        buf = engine->create_vertex_buffer(&tr[0], tr.size());
        if (buf == nullptr)
        {
            std::cerr << "can't create vertex buffer " << path << std::endl;
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

void static_object::update(const potap::vec2& p,
                    const float& asp,
                    const float& sc,
                    const float& t)
{

    pos = p;
    move = potap::mat2x3::move(pos);
    rot = potap::mat2x3::rotation(t);
    aspect = potap::mat2x3::scale(asp, sc);
}

void static_object::update_w_ani(const potap::vec2 &p,
                                 const float &asp,
                                 const float &sc,
                                 const float &t,
                                 const float &start_time)
    {

        if (play_ani)
        {
            //        float current_time = engine->get_time_from_init ();
            float delta_time = engine->get_time_from_init() - start_time;
            float one_frame_time = 0.2f;

            int how_may_frames_from_start_new = static_cast<int>(delta_time / one_frame_time);

            if (how_may_frames_from_start_new != hmffs_old)
            {
                if (how_may_frames_from_start_new <= 9)
                {
                   engine->destroy_vertex_buffer (buf);

                    float offset = static_cast<float>(how_may_frames_from_start_new / 10.f);

                    tr[0].v[0].uv.x = offset;
                    tr[0].v[1].uv.x = 0.1f + offset;
                    tr[0].v[2].uv.x = 0.1f + offset;
                    tr[1].v[0].uv.x = offset;
                    tr[1].v[1].uv.x = 0.1f + offset;
                    tr[1].v[2].uv.x = offset;

                    tr[0].v[0].uv.y = 0.f;
                    tr[0].v[1].uv.y = 0.f;
                    tr[0].v[2].uv.y = 1.f;
                    tr[1].v[0].uv.y = 0.f;
                    tr[1].v[1].uv.y = 1.f;
                    tr[1].v[2].uv.y = 1.f;

                    hmffs_old = how_may_frames_from_start_new;

                    if (how_may_frames_from_start_new == 9)
                    {
                        play_ani = false;
                        visible = false;
                    }

                    buf = engine->create_vertex_buffer (&tr[0], 2);

                }
            }
        }

        pos = p;
        move = potap::mat2x3::move(pos);
        rot = potap::mat2x3::rotation(t);
        aspect = potap::mat2x3::scale(asp, sc);
}

void static_object::update_w_texture(const potap::vec2 &p, const float &asp, const float &sc, const float &t, const float &value)
{
    engine->destroy_vertex_buffer (buf);
    int fuel_int = static_cast<int>(value / 10);
    float offset = static_cast<float>(fuel_int) / 10;
    if (offset >= 0.1f)
    {
        tr[0].v[0].uv.x = offset;
        tr[0].v[1].uv.x = offset;
        tr[0].v[2].uv.x = offset - 0.1f;
        tr[1].v[0].uv.x = offset;
        tr[1].v[1].uv.x = offset - 0.1f;;
        tr[1].v[2].uv.x = offset - 0.1f;;

        tr[0].v[0].uv.y = 0.f;
        tr[0].v[1].uv.y = 1.f;
        tr[0].v[2].uv.y = 1.f;
        tr[1].v[0].uv.y = 0.f;
        tr[1].v[1].uv.y = 1.f;
        tr[1].v[2].uv.y = 0.f;
    }

    buf = engine->create_vertex_buffer (&tr[0], 2);

    pos = p;
    move = potap::mat2x3::move(pos);
    rot = potap::mat2x3::rotation(t);
    aspect = potap::mat2x3::scale(asp, sc);
}

bool static_object::draw()
{
    if (!visible) {return false;}

    final_mat2x3 = rot * move * aspect;
    engine->render(*buf, tex, final_mat2x3);

    return EXIT_SUCCESS;
}

potap::vec2 static_object::spin_for(const potap::vec2 &pos, const float& k)
{
    potap::vec2 old_pos = pos;
    float speed = 5.f;
    if (old_pos.x > 1 || old_pos.y > 1){ speed = 0.5f;}
    float time_spin = engine->get_time_from_init() / k;
    float s    = std::sin(time_spin) / speed;
    float c    = std::cos(time_spin) / speed;

    return {old_pos.x + s, old_pos.y + c};
}

potap::vec2 static_object::spin_back(const potap::vec2 &pos, const float& k)
{
    potap::vec2 old_pos = pos;
    float time_spin = engine->get_time_from_init() / k;
    float c    = std::sin(time_spin) / 5;
    float s    = std::cos(time_spin) / 5;

    return {old_pos.x + s, old_pos.y + c};
}

bool static_object::set_position(const potap::vec2 &_position)
{
    static_object::pos = _position;
    return EXIT_SUCCESS;
}

void static_object::set_visible(const bool &v)
{
    visible = v;
}

potap::vec2 static_object::get_tex_coord()
{
    potap::vec2 tex_coord;
    tex_coord.x = tr[0].v[0].pos.x;
    tex_coord.y = tr[0].v[1].pos.y;
    return tex_coord;
}

float static_object::get_x()
{
    return pos.x;
}

float static_object::get_y()
{
    return pos.y;
}

potap::vec2 static_object::get_pos()
{
    return potap::vec2{pos.x, pos.y};
}
