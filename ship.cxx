#include <iostream>
#include <fstream>
#include <cmath>

#include "ship.hxx"

#define SHIP_FUEL_LOOSE 0.1f;
#define SHIP_SPEED_INCREASE_STEP 0.0001f;
#define SHIP_SPEED_DENCREASE_STEP 0.00005f;
#define SHIP_SPEED_MAX 0.015f;

bool ship::init(potap::engine* e,
                const std::string& f1,
                const std::string& f2,
                const std::string& path_to_tex_fire,
                const std::string& path_to_vert_buff_fire)
{
    engine = e;
    set_texture(f1);
    set_vertex_buffer(f2);
    fire.init(e, path_to_tex_fire, path_to_vert_buff_fire);
    crash.init(engine, "resourses/crash.png", "buffers/vert_tex_color_crash_new.txt");
    crash.set_visible (false);

    return EXIT_SUCCESS;
}

bool ship::set_texture(const std::string &t)
{
    tex = engine->create_texture(t);
    if (nullptr == tex)
    {
        std::cerr << "failed load texture " << t << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool ship::set_vertex_buffer(const std::string &path)
{
    buf = nullptr;

    std::ifstream file(path);
    if (!file)
    {
        std::cerr << "can't load " << path << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<potap::tri2, 2> tr;
        file >> tr[0] >> tr[1];

        buf = engine->create_vertex_buffer(&tr[0], tr.size());
        if (buf == nullptr)
        {
            std::cerr << "can't create vertex buffer " << path << std::endl;
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

void ship::update(const float& sc)
{
    move = potap::mat2x3::move(pos);
    rot = potap::mat2x3::rotation(direction);
    aspect = potap::mat2x3::scale(scale, sc);
}

void ship::update(const potap::vec2& p, const float& asp, const float& sc, const float& t)
{
    if (!visible)
    {
        pos = potap::vec2(-0.8f, 0.8f);
    }
    pos = p;
    move = potap::mat2x3::move(pos);
    rot = potap::mat2x3::rotation(t);
    aspect = potap::mat2x3::scale(asp, sc);
}


bool ship::touch(potap::vec2& pos_obj)
{
    if (sqrtf((pos.x - pos_obj.x)*(pos.x - pos_obj.x) +
              (pos.y - pos_obj.y)*(pos.y - pos_obj.y)) < 0.1f)
    {
        return true;
    }
    else {
        return false;
    }
}

bool ship::touch_and_explosive(static_object &object, static_object &explosive)
{
    if (sqrtf((pos.x - object.pos.x)*(pos.x - object.pos.x) +
              (pos.y - object.pos.y)*(pos.y - object.pos.y)) < 0.1f)
    {
        explosive.set_position (object.pos);
        set_position (object.pos);
        return true;
    }
    else {
        return false;
    }
}

bool ship::draw()
{
    if (!visible) {return false;}

    final_mat2x3 = rot * move * aspect;
    engine->render(*buf, tex, final_mat2x3);

    fire.draw();

    return EXIT_SUCCESS;
}

void ship::keyboard_events()
{
    float pi = 3.1415926f;
    potap::vec2 fire_pos;

//    if (fuel <= 0.f)
//    {
//        fire.set_visible (false);
//        return;
//    }

//    std::cout << "fuel = " << ship::fuel << std::endl;

    if (engine->is_key_down(potap::keys::left))
    {
        fire.pos.x = pos.x + 0.07f;
        fire.pos.y = pos.y;
        ship::fuel -= SHIP_FUEL_LOOSE;      
        speed_x_neg += SHIP_SPEED_INCREASE_STEP;
        direction = -pi / 2.f;
    } else {
        speed_x_neg -= SHIP_SPEED_DENCREASE_STEP;
    }

    if (engine->is_key_down(potap::keys::right))
    {
        fire.pos.x = pos.x - 0.07f;
        fire.pos.y = pos.y;
        ship::fuel -= SHIP_FUEL_LOOSE;
        speed_x_pos += SHIP_SPEED_INCREASE_STEP;
        direction = pi / 2.f;
    } else {
        speed_x_pos -= SHIP_SPEED_DENCREASE_STEP;
    }

    if (engine->is_key_down(potap::keys::up))
    {
        fire.pos.x = pos.x;
        fire.pos.y = pos.y - 0.07f;
        ship::fuel -= SHIP_FUEL_LOOSE;
        speed_y_pos += SHIP_SPEED_INCREASE_STEP;
        direction = 0.f;
    } else {
        speed_y_pos -= SHIP_SPEED_DENCREASE_STEP;
    }

    if (engine->is_key_down(potap::keys::down))
    {
        fire.pos.x = pos.x;
        fire.pos.y = pos.y + 0.07f;
        ship::fuel -= SHIP_FUEL_LOOSE;
        speed_y_neg += SHIP_SPEED_INCREASE_STEP;
        direction = -pi;
    } else {
        speed_y_neg -= SHIP_SPEED_DENCREASE_STEP;
    }

    if (speed_x_neg >= 0.015f) {speed_x_neg = SHIP_SPEED_MAX;}
    if (speed_x_neg <= 0.f) {speed_x_neg = 0.f;}
    if (speed_x_pos > 0.015f) {speed_x_pos = SHIP_SPEED_MAX;}
    if (speed_x_pos <= 0.f) {speed_x_pos = 0.f;}
    if (speed_y_pos > 0.015f) {speed_y_pos = SHIP_SPEED_MAX;}
    if (speed_y_pos <= 0.f) {speed_y_pos = 0.f;}
    if (speed_y_neg > 0.015f) {speed_y_neg = SHIP_SPEED_MAX;}
    if (speed_y_neg <= 0.f) {speed_y_neg = 0.f;}

    pos.x = pos.x + speed_x_pos - speed_x_neg;
    pos.y = pos.y + speed_y_pos - speed_y_neg;

    if (pos.x > 0.95f) {pos.x = 0.95f;}
    if (pos.x < -0.95f) {pos.x = -0.95f;}

    if (pos.y > 0.7f) {pos.y = 0.7f;}
    if (pos.y < -0.7f) {pos.y = -0.7f;}


///////////////////////
    if  ((engine->is_key_down(potap::keys::up)) && (engine->is_key_down(potap::keys::left)))
    {
        direction = 0.25f * -pi;//0.75f * pi;
//        fire_pos = potap::vec2(pos.x + 0.05f, pos.y - 0.05f);
        fire.pos.x = pos.x + 0.05f;
        fire.pos.y = pos.y - 0.05f;
    }
    if  ((engine->is_key_down(potap::keys::up)) && (engine->is_key_down(potap::keys::right)))
    {
        direction = 0.25f * pi;//0.75f * -pi;
//        fire_pos = potap::vec2(pos.x - 0.05f, pos.y - 0.05f);
        fire.pos.x = pos.x - 0.05f;
        fire.pos.y = pos.y - 0.05f;
    }
    if  ((engine->is_key_down(potap::keys::down)) && (engine->is_key_down(potap::keys::left)))
    {
        direction = 0.75f * -pi;//0.25f * pi;
//        fire_pos = potap::vec2(pos.x + 0.05f, pos.y + 0.05f);
        fire.pos.x = pos.x + 0.05f;
        fire.pos.y = pos.y + 0.05f;
    }
    if  ((engine->is_key_down(potap::keys::down)) && (engine->is_key_down(potap::keys::right)))
    {
        direction = 0.75f * pi;//0.25f * -pi;
//        fire_pos = potap::vec2(pos.x - 0.05f, pos.y + 0.05f);
        fire.pos.x = pos.x - 0.05f;
        fire.pos.y = pos.y + 0.05f;
    }
     if ((engine->is_key_down(potap::keys::left)) ||
         (engine->is_key_down(potap::keys::right)) ||
         (engine->is_key_down(potap::keys::up)) ||
         (engine->is_key_down(potap::keys::down)))
     {
        fire.set_visible (true);
     } else {
        fire.set_visible (false);
     }

//    return EXIT_SUCCESS;
}

void ship::new_coord (potap::vec2& current_ship_pos, potap::vec2 planet_pos, float mass)
{
    float x = 0.f;
    float y = 0.f;
    float m = mass;
    float lenght = sqrtf((current_ship_pos.x - planet_pos.x)*(current_ship_pos.x - planet_pos.x) +
                         (current_ship_pos.y - planet_pos.y)*(current_ship_pos.y - planet_pos.y));
    float x_100_part = std::abs((current_ship_pos.x - planet_pos.x) / 1000);
    float y_100_part = std::abs((current_ship_pos.y - planet_pos.y) / 1000);


    if ((current_ship_pos.x > planet_pos.x) && (current_ship_pos.y > planet_pos.y)){
        if (lenght > 0.8f*m) {x = 0; y = 0;}
        if (lenght <= 0.8f*m) {x = -x_100_part * 1; y = -y_100_part * 1;}
        if (lenght < 0.7f*m) {x = -x_100_part * 2; y = -y_100_part * 2;}
        if (lenght < 0.6f*m) {x = -x_100_part * 3; y = -y_100_part * 3;}
        if (lenght < 0.5f*m) {x = -x_100_part * 7; y = -y_100_part * 7;}
        if (lenght < 0.4f*m) {x = -x_100_part * 10; y = -y_100_part * 10;}
        if (lenght < 0.3f*m) {x = -x_100_part * 17; y = -y_100_part * 17;}
        if (lenght < 0.2f*m) {x = -x_100_part * 33; y = -y_100_part * 33;}
        if (lenght < 0.1f*m) {x = -x_100_part * 150; y = -y_100_part * 150;} }

    if ((current_ship_pos.x > planet_pos.x) && (current_ship_pos.y < planet_pos.y)){
        if (lenght > 0.8f*m) {x = 0; y = 0;}
        if (lenght <= 0.8f*m) {x = -x_100_part * 1; y = y_100_part * 1;}
        if (lenght < 0.7f*m) {x = -x_100_part * 2; y = y_100_part * 2;}
        if (lenght < 0.6f*m) {x = -x_100_part * 3; y = y_100_part * 3;}
        if (lenght < 0.5f*m) {x = -x_100_part * 7; y = y_100_part * 7;}
        if (lenght < 0.4f*m) {x = -x_100_part * 10; y = y_100_part * 10;}
        if (lenght < 0.3f*m) {x = -x_100_part * 17; y = y_100_part * 17;}
        if (lenght < 0.2f*m) {x = -x_100_part * 33; y = y_100_part * 33;}
        if (lenght < 0.1f*m) {x = -x_100_part * 150; y = y_100_part * 150;} }

    if ((current_ship_pos.x < planet_pos.x) && (current_ship_pos.y < planet_pos.y)){
        if (lenght > 0.8f*m) {x = 0; y = 0;}
        if (lenght <= 0.8f*m) {x = x_100_part * 1; y = y_100_part * 1;}
        if (lenght < 0.7f*m) {x = x_100_part * 2; y = y_100_part * 2;}
        if (lenght < 0.6f*m) {x = x_100_part * 3; y = y_100_part * 3;}
        if (lenght < 0.5f*m) {x = x_100_part * 7; y = y_100_part * 7;}
        if (lenght < 0.4f*m) {x = x_100_part * 10; y = y_100_part * 10;}
        if (lenght < 0.3f*m) {x = x_100_part * 17; y = y_100_part * 17;}
        if (lenght < 0.2f*m) {x = x_100_part * 33; y = y_100_part * 33;}
        if (lenght < 0.1f*m) {x = x_100_part * 150; y = y_100_part * 150;} }

    if ((current_ship_pos.x < planet_pos.x) && (current_ship_pos.y > planet_pos.y)){
        if (lenght > 0.8f*m) {x = 0; y = 0;}
        if (lenght <= 0.8f*m) {x = x_100_part * 1; y = -y_100_part * 1;}
        if (lenght < 0.7f*m) {x = x_100_part * 2; y = -y_100_part * 2;}
        if (lenght < 0.6f*m) {x = x_100_part * 3; y = -y_100_part * 3;}
        if (lenght < 0.5f*m) {x = x_100_part * 7; y = -y_100_part * 7;}
        if (lenght < 0.4f*m) {x = x_100_part * 10; y = -y_100_part * 10;}
        if (lenght < 0.3f*m) {x = x_100_part * 17; y = -y_100_part * 17;}
        if (lenght < 0.2f*m) {x = x_100_part * 33; y = -y_100_part * 33;}
        if (lenght < 0.1f*m) {x = x_100_part * 150; y = -y_100_part * 150;} }

    current_ship_pos.x += x * m;
    current_ship_pos.y += y * m;

//    return EXIT_SUCCESS;
}

void ship::set_visible(const bool &v)
{
    visible = v;
    fire.visible = v;
}

float ship::get_x()
{
    return pos.x;
}

float ship::get_y()
{
    return pos.y;
}

potap::vec2 ship::get_pos()
{
    return potap::vec2{pos.x, pos.y};
}
