#pragma once

#include "coordinates.hxx"

class gravity_point{
public:
    gravity_point() = default;
    gravity_point(const double& x_, const double& y_, const double& m_)
        : coord{x_, y_}
        , mass{m_}
    {}
    coordinates coord;
    double      mass;
};
