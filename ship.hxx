#pragma once

#include <array>
#include <memory>

#include "static_object.hxx"

class ship : public static_object
{
public:

    bool init (potap::engine* e,
              const std::string& path_to_tex,
              const std::string& path_to_vert_buff,
              const std::string& path_to_tex_fire,
              const std::string& path_to_vert_buff_fire);

    bool set_texture (const std::string& t);

    bool set_vertex_buffer (const std::string& path);

    void update(const float& sc);

    void update(const potap::vec2& p, const float& asp, const float& sc, const float& t);

    bool touch(potap::vec2& pos);

    bool touch_and_explosive(static_object& object, static_object& explosive);

    bool draw();

    void keyboard_events();

    void new_coord (potap::vec2&, potap::vec2, float);

    void set_visible(const bool& v);

    float get_x();
    float get_y();
    potap::vec2 get_pos();

    potap::vec2 pos{-0.8f, 0.8f};
    float fuel{100.0f};
    float direction{0.f};
    bool  visible{true};
    float speed_x_pos{0.f};
    float speed_x_neg{0.f};
    float speed_y_pos{0.f};
    float speed_y_neg{0.f};
    float scale{1.f};

    static_object fire;
    static_object crash;

};
