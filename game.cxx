#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <chrono>
#include <ctime>
#include <string_view>

#include "engine.hxx"
#include "static_object.hxx"
#include "ship.hxx"

#define SCALE_KOEF 1.333333f

int main(int /*argc*/, char* /*argv*/[])
{
    potap::engine* engine = potap::create_engine();

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }
//////////////////////////////////
    enum game_statement
    {
        menu, level_1, level_2, level_3, finish_treasure, finish_loose
    };
////////////////////////////////// init menu elements
    static_object main_menu;
    main_menu.init(engine, "resourses/main_menu.png", "buffers/vert_tex_black_fone.txt");

    static_object fone_win;
    fone_win.init(engine, "resourses/finish_black_fone_win.png", "buffers/vert_tex_black_fone.txt");

    static_object fone_loose;
    fone_loose.init(engine, "resourses/finish_black_fone_loose.png", "buffers/vert_tex_black_fone.txt");

    static_object name;
    name.init(engine, "resourses/game_name.png", "buffers/vert_tex_game_name.txt");

    static_object button_level_1;
    button_level_1.init(engine, "resourses/button_level_1.png", "buffers/vert_tex_button.txt");

    static_object button_level_2;
    button_level_2.init(engine, "resourses/button_level_2.png", "buffers/vert_tex_button.txt");

    static_object button_level_3;
    button_level_3.init(engine, "resourses/button_level_3.png", "buffers/vert_tex_button.txt");

    static_object button_menu;
    button_menu.init(engine, "resourses/button_menu.png", "buffers/vert_tex_button.txt");

    static_object button_replay;
    button_replay.init(engine, "resourses/button_replay.png", "buffers/vert_tex_button.txt");
/////////////////////////////////// initialize girls
    static_object girl_1;
    girl_1.init(engine, "resourses/girl1.png", "buffers/vert_tex_girl.txt");

    static_object girl_2;
    girl_2.init(engine, "resourses/girl2.png", "buffers/vert_tex_girl.txt");

    static_object girl_3;
    girl_3.init(engine, "resourses/girl3.png", "buffers/vert_tex_girl.txt");

    static_object dont_stop;
    dont_stop.init(engine, "resourses/dont_stop.png", "buffers/vert_tex_dont_stop.txt");


/////////////////////////////////// initialize elements
    static_object space;
    space.init(engine, "resourses/space3.png", "buffers/vert_tex_back.txt");

    static_object sun;
    sun.init(engine, "resourses/sun.png", "buffers/vert_tex_sun.txt");
    static_object sun_mine;
    sun_mine.init(engine, "resourses/mine_new.png", "buffers/vert_tex_mine.txt");

    static_object moon;
    moon.init(engine, "resourses/moon.png", "buffers/vert_tex_moon.txt");
    static_object moon_mine;
    moon_mine.init(engine, "resourses/mine_new.png", "buffers/vert_tex_mine.txt");

    static_object neptune;
    neptune.init(engine, "resourses/neptune.png", "buffers/vert_tex_neptune.txt");
    static_object neptune_mine;
    neptune_mine.init(engine, "resourses/mine_new.png", "buffers/vert_tex_mine.txt");

    static_object saturn;
    saturn.init(engine, "resourses/saturn.png", "buffers/vert_tex_saturn.txt");
    static_object saturn_mine;
    saturn_mine.init(engine, "resourses/mine_new.png", "buffers/vert_tex_mine.txt");

    static_object fireboll;
    fireboll.init(engine, "resourses/fireboll.png", "buffers/vert_tex_fireboll.txt");

    ship ship_body;
    ship_body.init(engine, "resourses/ship_body2.png", "buffers/vert_tex_color_ship.txt", "resourses/ship_fire.png", "buffers/vert_tex_color_ship_fire.txt");
    ship_body.fire.set_visible (false);

    static_object treasure;
    treasure.init(engine, "resourses/treasure2.png", "buffers/vert_tex_color_cosmonaut.txt");

    static_object cosmonaut;
    cosmonaut.init(engine, "resourses/cosmonaut4.png", "buffers/vert_tex_color_cosmonaut.txt");

    static_object cosmonaut2;
    cosmonaut2.init(engine, "resourses/cosmonaut4.png", "buffers/vert_tex_color_cosmonaut.txt");

    static_object cosmonaut3;
    cosmonaut3.init(engine, "resourses/cosmonaut4.png", "buffers/vert_tex_color_cosmonaut.txt");

    static_object base;
    base.init(engine, "resourses/house3.png", "buffers/vert_tex_color_base.txt");

    static_object fuel_view;
    fuel_view.init(engine, "resourses/fuel.png", "buffers/vert_tex_fuel.txt");

///////////////////////////////////////// create sounds
    potap::sound_buffer* music =
        engine->create_sound_buffer("resourses/audio/song_for_level_less_sound.wav");
    potap::sound_buffer* expl =
        engine->create_sound_buffer("resourses/audio/explosive4_high_sound.wav");
    potap::sound_buffer* cosm =
        engine->create_sound_buffer("resourses/audio/voice_short.wav");
    potap::sound_buffer* treas =
        engine->create_sound_buffer("resourses/audio/treasure.wav");
    potap::sound_buffer* ovacii =
        engine->create_sound_buffer("resourses/audio/ovacii.wav");
    potap::sound_buffer* loose_level =
        engine->create_sound_buffer("resourses/audio/loose_level.wav");
    potap::sound_buffer* fire_sound =
        engine->create_sound_buffer("resourses/audio/fire.wav");
    potap::sound_buffer* sputnik =
        engine->create_sound_buffer("resourses/audio/sputnik_short.wav");
    potap::sound_buffer* start_level =
        engine->create_sound_buffer("resourses/audio/start_level.wav");
    potap::sound_buffer* dont_stop_sweety =
        engine->create_sound_buffer("resourses/audio/dontstop2.wav");
    potap::sound_buffer* fuel_5 =
        engine->create_sound_buffer("resourses/audio/5procent.wav");
    potap::sound_buffer* fuel_25 =
        engine->create_sound_buffer("resourses/audio/25procent.wav");
    potap::sound_buffer* fuel_50 =
        engine->create_sound_buffer("resourses/audio/50procent.wav");
    potap::sound_buffer* fuel_75 =
        engine->create_sound_buffer("resourses/audio/75procent.wav");


    assert(treas != nullptr);
    assert(ovacii != nullptr);
    assert(cosm != nullptr);
    assert(music != nullptr);
    assert(expl != nullptr);
    assert(fire_sound != nullptr);
    assert(dont_stop_sweety != nullptr);
    assert(fuel_5 != nullptr);
    assert(fuel_25 != nullptr);
    assert(fuel_50 != nullptr);
    assert(fuel_75 != nullptr);

/////////////////////////////////////////

    int game_current_state = menu;
    bool continue_loop = true;
    float start_delta_time_iteration = engine->get_time_from_init ();
    potap::mouse_pos mouse_real;
    int level_replay;
    ship_body.crash.start_time = 0.f;

//////////////////////////////////////////////
// START LOOP   START LOOP    START LOOP
/////////////////////////////////////////////
    while (continue_loop)
    {
        potap::event event;
        engine->read_event(event);


        if (event == potap::event::turn_off)
        {
            continue_loop = false;
        }

//        if (engine->is_key_down(potap::keys::escape))
//        {
//            game_current_state = menu;
//        }

/////////////////////////////////////
        mouse_real = engine->get_mouse ();

/////////////////////////////////////
        if (game_current_state == menu)
        {
            music->play(potap::sound_buffer::properties::stop);

            if (!sputnik->is_playing)
            {
                sputnik->play(potap::sound_buffer::properties::once);
            }

            if (event == potap::event::mouse_pressed)
            {
                if (mouse_real.x >= 320 && mouse_real.x <= 470)
                {
                    if (mouse_real.y >= 270 && mouse_real.y <= 320)
                    {
                        game_current_state = level_1;
                        start_level->play(potap::sound_buffer::properties::once);
                    }
                    if (mouse_real.y >= 350 && mouse_real.y <= 400)
                    {
                        game_current_state = level_2;
                        start_level->play(potap::sound_buffer::properties::once);
                    }
                    if (mouse_real.y >= 430 && mouse_real.y <= 480)
                    {
                        game_current_state = level_3;
                        start_level->play(potap::sound_buffer::properties::once);
                    }
                }
            }
        }

/////////////////////////////////////
        if (game_current_state == finish_loose || game_current_state == finish_treasure)
        {
            music->play(potap::sound_buffer::properties::stop);

            if (event == potap::event::mouse_pressed)
            {               
                if (mouse_real.y >= 450 && mouse_real.y <= 580)
                {
                    if (mouse_real.x >= 100 && mouse_real.x <= 350)
                    {
                        game_current_state = menu;
                    }
                    if (mouse_real.x >= 450 && mouse_real.x <= 750)
                    {
                        game_current_state = level_replay;
                    }
                }
            }
        }

///////////////////////////////////// ship change position from buttons 1/100 sec

        if ((game_current_state != finish_loose) &&
            (game_current_state != finish_treasure) &&
            (game_current_state != menu))
        {
            if (!music->is_playing)
            {
            music->play(potap::sound_buffer::properties::once);
            }
////////////////////////////////////////
            if (ship_body.fuel < 75.2f && ship_body.fuel > 75.0f)
            {
                fuel_75->play(potap::sound_buffer::properties::once);
            }

            if (ship_body.fuel < 50.2f && ship_body.fuel > 50.0f)
            {
                fuel_50->play(potap::sound_buffer::properties::once);
            }

            if (ship_body.fuel < 25.2f && ship_body.fuel > 25.0f)
            {
                fuel_25->play(potap::sound_buffer::properties::once);
            }

            if (ship_body.fuel < 10.2f && ship_body.fuel > 10.0f)
            {
                fuel_5->play(potap::sound_buffer::properties::once);
            }
////////////////////////////////////// start iteration
            float end_delta_time_iteration = engine->get_time_from_init ();
            if ((end_delta_time_iteration - start_delta_time_iteration) > 0.017f)
            { //start 1/50 sec

                start_delta_time_iteration = end_delta_time_iteration;
//////////////////////////////////////
                ship_body.keyboard_events();// moving ship_body

////////////////////////////////////// gravity  ship <-> static_object
                ship_body.new_coord(ship_body.pos, sun.get_pos(), 1.3f);
                ship_body.new_coord(ship_body.pos, moon.get_pos(), 1.0f);
                ship_body.new_coord(ship_body.pos, saturn.get_pos(), 1.1f);
                ship_body.new_coord(ship_body.pos, neptune.get_pos(), 0.8f);

//                if (ship_body.fuel > 99.5f){ship_body.set_position({-0.8f, 0.8f});}

////////////////////////////////////// touch with objects

                ship_body.touch_and_explosive (sun, ship_body.crash);
                if (ship_body.touch_and_explosive (sun_mine, ship_body.crash))
                {
                    sun_mine.set_visible (false);
                }

                ship_body.touch_and_explosive (saturn, ship_body.crash);
                if (ship_body.touch_and_explosive (saturn_mine, ship_body.crash))
                {
                    saturn_mine.set_visible (false);
                }

                ship_body.touch_and_explosive (neptune, ship_body.crash);
                if (ship_body.touch_and_explosive (neptune_mine, ship_body.crash))
                {
                    neptune_mine.set_visible (false);
                }

                ship_body.touch_and_explosive (moon, ship_body.crash);
                if (ship_body.touch_and_explosive (moon_mine, ship_body.crash))
                {
                    moon_mine.set_visible (false);
                }

                ship_body.touch_and_explosive (fireboll, ship_body.crash);

///////////////////////////////////////////  crash animation
                if (ship_body.touch(sun.pos) ||
                    ship_body.touch(saturn.pos) ||
                    ship_body.touch(neptune.pos) ||
                    ship_body.touch(moon.pos) ||
                    ship_body.touch(moon_mine.pos) ||
                    ship_body.touch(sun_mine.pos) ||
                    ship_body.touch(saturn_mine.pos) ||
                    ship_body.touch(neptune_mine.pos) ||
                    ship_body.touch(fireboll.pos))
                {
                    expl->play(potap::sound_buffer::properties::once);
                    ship_body.crash.play_ani = true;
                    ship_body.crash.set_visible (true);
                    ship_body.set_visible(false);
                }

                if (ship_body.crash.play_ani && (ship_body.crash.start_time == 0.f))
                {
                   ship_body.crash.start_time = engine->get_time_from_init ();
                }

///////////////////////////////////// touch with cosmonaut
                if (ship_body.touch(cosmonaut.pos) &&
                   (cosmonaut.visible == true))
                {
                    cosmonaut.update({3.f, 3.f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
                    cosmonaut.set_visible(false);
                    cosm->play(potap::sound_buffer::properties::once);
                }
///////////////////////////////////// touch with cosmonaut
                if (ship_body.touch(cosmonaut2.pos) &&
                   (cosmonaut2.visible == true))
                {
                    cosmonaut2.update({3.f, 3.f}, ship_body.scale, SCALE_KOEF, 0);
                    cosmonaut2.set_visible(false);
                    cosm->play(potap::sound_buffer::properties::once);
                }
///////////////////////////////////// touch with cosmonaut
                if (ship_body.touch(cosmonaut3.pos) &&
                   (cosmonaut3.visible == true))
                {
                    cosmonaut3.update({3.f, 3.f}, ship_body.scale, SCALE_KOEF, 0);
                    cosmonaut3.set_visible(false);
                    cosm->play(potap::sound_buffer::properties::once);
                }
/////////////////////////////////// touch with treasure
                if (ship_body.touch(treasure.pos) &&
                   (treasure.visible == true))
                {
                    treasure.update({3.f, 3.f}, ship_body.scale, SCALE_KOEF, 0);
                    treasure.set_visible(false);
                    treas->play(potap::sound_buffer::properties::once);
                }

/////////////////////////////////// finish level
                if ((!expl->is_playing && !ship_body.visible) || (ship_body.fuel <= 0.1f))
                {
                    loose_level->play(potap::sound_buffer::properties::once);
                    game_current_state = finish_loose;
                }
/////////////////////////////////////// fire sound
                if ((!fire_sound->is_playing) && ship_body.fire.visible && ship_body.visible)
                {
                    fire_sound->play(potap::sound_buffer::properties::once);
                }
//////////////////////////////////////// touch with base after take all cosmonaut
                if (ship_body.touch(base.pos))
                {
                    if ((ship_body.visible && !cosmonaut.visible && !cosmonaut2.visible && !cosmonaut3.visible))
                    {
                        ship_body.set_visible(false);
//                        ship_body.update(base.pos, ship_body.scale, SCALE_KOEF, 0.f);
                        if (treasure.visible == false)
                            {game_current_state = finish_treasure;}
                        else {
                            ovacii->play(potap::sound_buffer::properties::once);
                            game_current_state = finish_loose;
                        }
                    }
                }
//////////////////////////////////
            }  // end 1/100
        }

///////////////////////////////////
        switch (game_current_state)
        {
        case menu:
            ship_body.set_position ({-0.8f, 0.8f});

            main_menu.update({0.f, 0.f}, 1.f, SCALE_KOEF, 0.f);
            main_menu.draw ();
            name.update({0.f, 0.4f}, ship_body.scale, SCALE_KOEF, 0.f);
            name.draw ();

            button_level_1.update({0.f, 0.0f}, ship_body.scale, SCALE_KOEF, 0.f);
            button_level_1.draw ();
            button_level_2.update({0.f, -0.2f}, ship_body.scale, SCALE_KOEF, 0.f);
            button_level_2.draw ();
            button_level_3.update({0.f, -0.4f}, ship_body.scale, SCALE_KOEF, 0.f);
            button_level_3.draw ();

            break;

        case level_1:
            level_replay = level_1;

            space.update({0, 0}, ship_body.scale, SCALE_KOEF, 0);
            space.draw();

            sun.update({0.7f, -0.6f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun.draw();
            sun_mine.update(sun_mine.spin_for(sun.pos, 1.3f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun_mine.draw();

            moon.update({0.6f, 0.f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon.draw();
            moon_mine.update(moon_mine.spin_for(moon.pos, 2.f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon_mine.draw();

            neptune.update({-0.6f, -0.6f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune.draw();
            neptune_mine.update(neptune_mine.spin_back(neptune.pos, 1.f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune_mine.draw();

            saturn.update({0.4f, 0.6f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn.draw();
            saturn_mine.update(saturn_mine.spin_back(saturn.pos, 1.8f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn_mine.draw();

            fireboll.update(fireboll.spin_for({1.5f, 1.5f}, 2.f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()/4));
            fireboll.draw();

            base.update({-0.8f, 0.f}, ship_body.scale, SCALE_KOEF, 0);
            base.draw();

            cosmonaut.update({0.f, -0.3f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            cosmonaut.draw();

            cosmonaut2.update({0.1f, 0.f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut2.draw();

            cosmonaut3.update({-0.5f, 0.1f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut3.draw();

            treasure.update({-0.3f, -0.2f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            treasure.draw();

            ship_body.crash.update_w_ani (ship_body.crash.pos, ship_body.scale, SCALE_KOEF, 0.f, ship_body.crash.start_time);
            ship_body.crash.draw ();

            ship_body.update(ship_body.pos, ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.fire.update(ship_body.fire.get_pos (), ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.draw ();

            fuel_view.update_w_texture({0.f, 0.7f}, ship_body.scale, SCALE_KOEF, 0.f, ship_body.fuel);
            fuel_view.draw();

            break;

        case level_2:
            level_replay = level_2;

            space.update({0, 0}, 1.f, SCALE_KOEF, 0);
            space.draw();

            sun.update({-0.5f, -0.4f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun.draw();
            sun_mine.update(sun_mine.spin_for(sun.pos, 1.3f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun_mine.draw();

            moon.update({0.3f, 0.3f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon.draw();
            moon_mine.update(moon_mine.spin_for(moon.pos, 2.f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon_mine.draw();

            neptune.update({0.5f, -0.5f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune.draw();
            neptune_mine.update(neptune_mine.spin_back(neptune.pos, 1.f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune_mine.draw();

            saturn.update({-0.25f, 0.2f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn.draw();
            saturn_mine.update(saturn_mine.spin_back(saturn.pos, 1.8f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn_mine.draw();

            fireboll.update(fireboll.spin_for({1.5f, -1.5f}, 1.5f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()/4));
            fireboll.draw();

            base.update({-0.8f, 0.f}, ship_body.scale, SCALE_KOEF, 0);
            base.draw();

            cosmonaut.update({0.7f, -0.4f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            cosmonaut.draw();

            cosmonaut2.update({0.5f, 0.65f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut2.draw();

            cosmonaut3.update({-0.5f, 0.2f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut3.draw();

            treasure.update({-0.5f, -0.2f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            treasure.draw();

            ship_body.crash.update_w_ani (ship_body.crash.pos, ship_body.scale, SCALE_KOEF, 0.f, ship_body.crash.start_time);
            ship_body.crash.draw ();

            ship_body.update(ship_body.pos, ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.fire.update(ship_body.fire.get_pos (), ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.draw ();

            fuel_view.update_w_texture({0.f, 0.7f}, ship_body.scale, SCALE_KOEF, 0.f, ship_body.fuel);
            fuel_view.draw();

            break;

        case level_3:

            level_replay = level_3;

            space.update({0, 0}, 1.f, SCALE_KOEF, 0);
            space.draw();

            sun.update({0.6f, 0.6f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun.draw();
            sun_mine.update(sun_mine.spin_for(sun.pos, 1.3f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()));
            sun_mine.draw();

            moon.update({-0.6f, -0.6f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon.draw();
            moon_mine.update(moon_mine.spin_for(moon.pos, 2.f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()*4));
            moon_mine.draw();

            neptune.update({0.2f, 0.2f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune.draw();
            neptune_mine.update(neptune_mine.spin_back(neptune.pos, 1.f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*3));
            neptune_mine.draw();

            saturn.update({-0.2f, -0.2f}, ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn.draw();
            saturn_mine.update(saturn_mine.spin_back(saturn.pos, 1.8f), ship_body.scale, SCALE_KOEF, (engine->get_time_from_init()*2));
            saturn_mine.draw();

            fireboll.update(fireboll.spin_for({-1.f, 1.5f}, 1.f), ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init()/4));
            fireboll.draw();

            base.update({0.8f, -0.6f}, ship_body.scale, SCALE_KOEF, 0);
            base.draw();

            cosmonaut.update({0.4f, 0.4f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            cosmonaut.draw();

            cosmonaut2.update({0.f, 0.f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut2.draw();

            cosmonaut3.update({-0.4f, -0.4f}, ship_body.scale, SCALE_KOEF, -(engine->get_time_from_init())*2);
            cosmonaut3.draw();

            treasure.update({-0.6f, -0.2f}, ship_body.scale, SCALE_KOEF, engine->get_time_from_init());
            treasure.draw();

            ship_body.crash.update_w_ani (ship_body.crash.pos, ship_body.scale, SCALE_KOEF, 0.f, ship_body.crash.start_time);
            ship_body.crash.draw ();

            ship_body.update(ship_body.pos, ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.fire.update(ship_body.fire.get_pos (), ship_body.scale, SCALE_KOEF, ship_body.direction);
            ship_body.draw ();

            fuel_view.update_w_texture({0.f, 0.7f}, ship_body.scale, SCALE_KOEF, 0.f, ship_body.fuel);
            fuel_view.draw();

            break;

        case finish_loose:

            ship_body.set_position ({-0.8f, 0.8f});
            ship_body.speed_x_pos = 0.f;
            ship_body.speed_y_pos = 0.f;
            ship_body.speed_x_neg = 0.f;
            ship_body.speed_y_neg = 0.f;
            ship_body.set_visible (true);
            ship_body.fuel = 100.f;
            sun_mine.set_visible (true);
            saturn_mine.set_visible (true);
            neptune_mine.set_visible (true);
            moon_mine.set_visible (true);
            cosmonaut.set_visible (true);
            cosmonaut2.set_visible (true);
            cosmonaut3.set_visible (true);
            treasure.set_visible (true);
            ship_body.crash.start_time = 0.f;

            fone_loose.update({0.f, 0.f}, 1.f, SCALE_KOEF, 0);
            fone_loose.draw();
            button_menu.update({-0.3f, -0.6f}, 1.f, SCALE_KOEF, 0);
            button_menu.draw();
            button_replay.update({0.3f, -0.6f}, 1.f, SCALE_KOEF, 0);
            button_replay.draw();

            break;

        case finish_treasure:
            if (treasure.visible == false && !dont_stop_sweety->is_playing)
            {
                dont_stop_sweety->play(potap::sound_buffer::properties::once);
            }
            ship_body.set_position ({-0.8f, 0.8f});
            ship_body.speed_x_pos = 0.f;
            ship_body.speed_y_pos = 0.f;
            ship_body.speed_x_neg = 0.f;
            ship_body.speed_y_neg = 0.f;
            ship_body.set_visible (true);
            ship_body.fuel = 100.f;
            sun_mine.set_visible (true);
            saturn_mine.set_visible (true);
            neptune_mine.set_visible (true);
            moon_mine.set_visible (true);
            cosmonaut.set_visible (true);
            cosmonaut2.set_visible (true);
            cosmonaut3.set_visible (true);
            treasure.set_visible (true);
            ship_body.crash.start_time = 0.f;

            fone_win.update({0.f, 0.f}, 1.f, SCALE_KOEF, 0);
            fone_win.draw();
            girl_3.update({0.f, -0.1f}, 1.f, SCALE_KOEF, 0);
            girl_3.draw();
            dont_stop.update({0.f, 0.6f}, 1.f, SCALE_KOEF, 0);
            dont_stop.draw();
            button_menu.update({-0.3f, -0.6f}, 1.f, SCALE_KOEF, 0);
            button_menu.draw();
            button_replay.update({0.3f, -0.6f}, 1.f, SCALE_KOEF, 0);
            button_replay.draw();

            break;
        }

//////////////////////////////////
        engine->swap_buffers();

    } // end LOOP

    engine->uninitialize();
    potap::destroy_engine(engine);

    return EXIT_SUCCESS;
}


