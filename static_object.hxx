#pragma once

#include <array>
#include <memory>

#include "graph_obj.hxx"
#include "engine.hxx"

class static_object : public graph_obj
{
public:
    potap::engine* engine;
    potap::texture* tex;
    potap::vertex_buffer* buf;
    std::array<potap::tri2, 2> tr;

    potap::mat2x3 final_mat2x3;
    potap::vec2   pos;
    potap::mat2x3 move;
    potap::mat2x3 aspect;
    potap::mat2x3 rot;

    bool play_ani = false;
    float start_time = 0.f;
    int hmffs_old = 0;
    bool visible;

    bool init (potap::engine* e,
              const std::string& path_to_tex,
              const std::string& path_to_vert_buff);

    bool set_texture (const std::string& t);

    bool set_vertex_buffer (const std::string& path);

    void update(const potap::vec2& p,
                const float& asp,
                const float& sc,
                const float& t);
    void update_w_ani(const potap::vec2& p,
                      const float& asp,
                      const float& sc,
                      const float& t,
                      const float& start_time);

    void update_w_texture(const potap::vec2& p,
                      const float& asp,
                      const float& sc,
                      const float& t,
                      const float& value);

    bool draw();

    potap::vec2 spin_for(const potap::vec2 &pos, const float& k); // 1<k<2
    potap::vec2 spin_back(const potap::vec2 &pos, const float& k);

    bool set_position(const potap::vec2& star_position);

    void set_visible(const bool& v);

    potap::vec2 get_pos();

    potap::vec2 get_tex_coord();

    float get_x();
    float get_y();

};
