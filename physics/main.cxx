#include "physics_object.hxx"

int main(int /*argc*/, char** /*argv*/){

    // object in center of coordinates with 1kg mass
    physics_object A ({0, 0}, 1);

    // add Earth, as gravity point (x, y, m)
    A.add_new_gravity_point({0, -6.378e6, 5.9726e24});
    // add Earth, as gravity point (x, y, m)
    A.add_new_gravity_point({-6.378e6 * 2, 6.378e6 * 2, 5.9726e24});

    A.update();

    A.print_forces();

    return 0;
}
