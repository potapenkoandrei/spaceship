#pragma once

#include <cmath>

#include "gravity_point.hxx"

class force{
public:
    force() = default;
    force(const coordinates& b, const coordinates& e, const double& a, const double& s)
        : beg{b}
        , end{e}
        , angle{a}
        , size{s}
    {}

    void clear(){
        beg.x = 0;
        beg.y = 0;
        end.x = 0;
        end.y = 0;
        angle = 0;
        size  = 0;
    }

    double get_angle_grad(){
        return angle * 180.0 / PI;
    }

// main logic should be here - its summarizing of 2 forces
    const force operator+(const force& rv) {

        //define shifts by x&y
        double delta_x = (end.x - beg.x) + (rv.end.x - rv.beg.x);
        double delta_y = (end.y - beg.y) + (rv.end.y - rv.beg.y);
        //define distance and angle in radians
        double s = std::sqrt(std::pow(delta_x, 2) + std::pow(delta_y, 2));
        double a = atan2(delta_y, delta_x);
        coordinates b {rv.beg.x, rv.beg.y};
        coordinates e {s * std::cos(a), s * std::sin(a)};

        return {b, e, a, s};
    }

    coordinates beg;
    coordinates end;
    double      angle;
    double      size;
};
