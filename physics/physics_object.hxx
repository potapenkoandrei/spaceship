#pragma once

#include <vector>
#include <cmath>

#include "force.hxx"

class physics_object {
public:
    physics_object() = default;
    physics_object(const coordinates& c, const double& m)
        : coord{c}
        , mass{m}
    {
        f.clear();
        g = 0;
    }
    virtual ~physics_object();

    int add_new_gravity_point(const gravity_point& p);
    int add_new_force(const force& f);
    double calculate_distance(const gravity_point& gp);
    force gravity_force_calculating(const gravity_point& gp);
    int update();
    void print_forces();

    coordinates coord;
    double mass;
    force f;
    double g;
    std::vector<gravity_point> points;
    std::vector<force> forces;
};
