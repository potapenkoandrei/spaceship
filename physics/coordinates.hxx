#pragma once

#include <iostream>

static double GRAVITY_CONST = 6.6720e-11;
static double GRAVITY_EARTH = 9.81;
static double PI = 3.1415926;

class coordinates {
public:
    coordinates() = default;
    coordinates(const double& x_, const double& y_)
        : x {x_}, y{y_}
    {}

    void print() {std::cout << "x= " << x << "y= " << y << std::endl;}

    double x;
    double y;
};
